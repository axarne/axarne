<h1 align="left">Hey 👋 What's up?</h1>

###

<p align="left">My name is Michael/Anthony and I'm from Germany but currently living in Poland</p>

###

<div align="left">
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/php/php-original.svg" height="40" alt="php logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/csharp/csharp-original.svg" height="40" alt="csharp logo"  />
  <img width="12" />
  <img src="https://upload.wikimedia.org/wikipedia/commons/4/40/VB.NET_Logo.svg" height="40" alt="vb.net logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/visualstudio/visualstudio-plain.svg" height="40" alt="visualstudio logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/debian/debian-original.svg" height="40" alt="debian logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/windows8/windows8-original.svg" height="40" alt="windows8 logo"  />
</div>

###

<h2 align="left">Contact me via</h2>

###

<div align="left">
  <a href="https://twitch.tv/AXARNE" target="_blank">
    <img src="https://raw.githubusercontent.com/maurodesouza/profile-readme-generator/master/src/assets/icons/social/twitch/default.svg" width="52" height="40" alt="twitch logo"  />
  </a>
  <a href="mailto:support@axarne.dev" target="_blank">
    <img src="https://raw.githubusercontent.com/maurodesouza/profile-readme-generator/master/src/assets/icons/social/microsoft-outlook/default.svg" width="52" height="40" alt="microsoft-outlook logo"  />
</div>

###

```
Donate via crypto if u wanna :3
Magi [XMG]: 98Cgq5DVRmivHU5iFyPrJrY3NjLRcHtq4V
Scala [XLA]: Svm4JPv13jL7584JWjjQkLM2LpyWAaGM6MZzzvfGEZSn9SHtYGcGnwZB2P44rj4H28LNTeQUGaw2hZShA9jRqewd2cpNbNBvH
```

###

<!--
<div align="center">
  <img src="https://profile-counter.glitch.me/AXARNE/count.svg?"  />
</div>

###

**axarne/axarne** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
